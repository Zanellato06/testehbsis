       IDENTIFICATION DIVISION.
       PROGRAM-ID.    MENU.
       SECURITY.
          *
          * Menu geral para acesso
          *
          *************************

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. PC.
       OBJECT-COMPUTER. PC.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  WS-OPCAO                  PIC 9(004)    VALUE ZEROS.
       01  WS-ED-OPCAO               PIC 99.99     VALUE ZEROS.
       01  WS-TECLA                  PIC 9(002)    VALUE ZEROS.
           88  WS-ESC                              VALUE 01.

       SCREEN SECTION.
           COPY "menu.scr".

       PROCEDURE DIVISION.
       0000-PRINCIPAL SECTION.
       0000.
          PERFORM 0100-MONTA-TELA
          PERFORM 0200-ACEITA-OPCAO WITH TEST AFTER UNTIL WS-ESC
          .
       0000-FIM.
           EXIT PROGRAM
           STOP RUN
           .

       0100-MONTA-TELA SECTION.
       0100.
           DISPLAY SC-TELA-PRINCIPAL
           .
       0100-FIM.
           EXIT
           .

       0200-ACEITA-OPCAO SECTION.
       0200.
           MOVE ZEROS                    TO WS-ED-OPCAO

           ACCEPT WS-ED-OPCAO AT 1519 WITH AUTO REVERSE-VIDEO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF NOT WS-ESC
               MOVE WS-ED-OPCAO          TO WS-OPCAO
               EVALUATE WS-OPCAO
                   WHEN 0101  CALL   "cadcli"
                              CANCEL "cadcli"
                              PERFORM 0100-MONTA-TELA
                   WHEN 0102  CALL   "cadven"
                              CANCEL "cadven"
                              PERFORM 0100-MONTA-TELA
                   WHEN 0201  CALL   "relcli"
                              CANCEL "relcli"
                              PERFORM 0100-MONTA-TELA
                   WHEN 0202  CALL   "relven"
                              CANCEL "relven"
                              PERFORM 0100-MONTA-TELA
                   WHEN 0301  CALL   "expcli"
                              CANCEL "expcli"
                              PERFORM 0100-MONTA-TELA
                   WHEN OTHER DISPLAY ">> Opção inválida. Verifique!!" AT 2310 WITH BEEP
                              ACCEPT WS-ESPERA WITH NO-ECHO
               END-EVALUATE
           END-IF
           .
       0200-FIM.
           EXIT
           .
