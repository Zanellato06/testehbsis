       IDENTIFICATION DIVISION.
       PROGRAM-ID.    VALIDAR.
       SECURITY.
          *
          * Rotinas de genéricas de validação
          *
          ************************************

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. PC.
       OBJECT-COMPUTER. PC.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  WS-SOMA                   PIC 9(004)    VALUE ZEROS.
       01  WS-QUOCIENTE              PIC 9(004)    VALUE ZEROS.
       01  WS-RESTO                  PIC 9(002)    VALUE ZEROS.
       01  WS-DIGITO                 PIC 9(001)    VALUE ZEROS.

       01  WS-NROS-INVALIDOS         PIC 9(014)    VALUE ZEROS.
           88  CNPJS-INVALIDOS                     VALUE 0
                                                         11111111111111
                                                         22222222222222
                                                         33333333333333
                                                         44444444444444
                                                         55555555555555
                                                         66666666666666
                                                         77777777777777
                                                         88888888888888
                                                         99999999999999.
           88  CPFS-INVALIDOS                      VALUE 0
                                                         11111111111
                                                         22222222222
                                                         33333333333
                                                         44444444444
                                                         55555555555
                                                         66666666666
                                                         77777777777
                                                         88888888888
                                                         99999999999.

       LINKAGE SECTION.
           COPY "validar.lnk".

       PROCEDURE DIVISION.
       0000-PRINCIPAL SECTION.
       0000.
           SET LK-VALIDO TO TRUE

           EVALUATE TRUE
               WHEN LK-VALIDAR-CNPJ   PERFORM 0100-CNPJ
               WHEN LK-VALIDAR-CPF    PERFORM 0200-CPF
               WHEN OTHER             SET LK-VALIDO TO FALSE
           END-EVALUATE
           .
       0000-FIM.
           EXIT PROGRAM
           STOP RUN
           .

       0100-CNPJ SECTION.
       0100.
           IF CNPJS-INVALIDOS
               SET LK-VALIDO TO FALSE
           ELSE
               COMPUTE WS-SOMA = (WS-DIG-CNPJ(01) * 5) +
                                 (WS-DIG-CNPJ(02) * 4) +
                                 (WS-DIG-CNPJ(03) * 3) +
                                 (WS-DIG-CNPJ(04) * 2) +
                                 (WS-DIG-CNPJ(05) * 9) +
                                 (WS-DIG-CNPJ(06) * 8) +
                                 (WS-DIG-CNPJ(07) * 7) +
                                 (WS-DIG-CNPJ(08) * 6) +
                                 (WS-DIG-CNPJ(09) * 5) +
                                 (WS-DIG-CNPJ(10) * 4) +
                                 (WS-DIG-CNPJ(11) * 3) +
                                 (WS-DIG-CNPJ(12) * 2)
               DIVIDE WS-SOMA BY 11 GIVING    WS-QUOCIENTE
                                    REMAINDER WS-RESTO
               IF WS-RESTO NOT LESS 02
                   SUBTRACT WS-RESTO FROM 11 GIVING WS-DIGITO
               ELSE
                   MOVE ZEROS                    TO WS-DIGITO
               END-IF

               IF WS-DIGITO NOT EQUAL WS-DIG-CNPJ(13)
                   SET LK-VALIDO TO FALSE
               ELSE
                   COMPUTE WS-SOMA = (WS-DIG-CNPJ(01) * 6) +
                                     (WS-DIG-CNPJ(02) * 5) +
                                     (WS-DIG-CNPJ(03) * 4) +
                                     (WS-DIG-CNPJ(04) * 3) +
                                     (WS-DIG-CNPJ(05) * 2) +
                                     (WS-DIG-CNPJ(06) * 9) +
                                     (WS-DIG-CNPJ(07) * 8) +
                                     (WS-DIG-CNPJ(08) * 7) +
                                     (WS-DIG-CNPJ(09) * 6) +
                                     (WS-DIG-CNPJ(10) * 5) +
                                     (WS-DIG-CNPJ(11) * 4) +
                                     (WS-DIG-CNPJ(12) * 3) +
                                     (WS-DIG-CNPJ(13) * 2)
                   DIVIDE WS-SOMA BY 11 GIVING    WS-QUOCIENTE
                                        REMAINDER WS-RESTO
                   IF WS-RESTO NOT LESS 02
                       SUBTRACT WS-RESTO FROM 11 GIVING WS-DIGITO
                   ELSE
                       MOVE ZEROS                TO WS-DIGITO
                   END-IF

                   IF WS-DIGITO NOT EQUAL WS-DIG-CNPJ(14)
                       SET LK-VALIDO TO FALSE
                   END-IF
               END-IF
           END-IF
           .
       0100-FIM.
           EXIT
           .

       0200-CPF SECTION.
       0200.
           IF CPFS-INVALIDOS
               SET LK-VALIDO TO FALSE
           ELSE
               COMPUTE WS-SOMA = (WS-DIG-CPF(01) * 10) +
                                 (WS-DIG-CPF(02) *  9) +
                                 (WS-DIG-CPF(03) *  8) +
                                 (WS-DIG-CPF(04) *  7) +
                                 (WS-DIG-CPF(05) *  6) +
                                 (WS-DIG-CPF(06) *  5) +
                                 (WS-DIG-CPF(07) *  4) +
                                 (WS-DIG-CPF(08) *  3) +
                                 (WS-DIG-CPF(09) *  2)
               DIVIDE WS-SOMA BY 11 GIVING    WS-QUOCIENTE
                                    REMAINDER WS-RESTO
               IF WS-RESTO NOT LESS 02
                   SUBTRACT WS-RESTO FROM 11 GIVING WS-DIGITO
               ELSE
                   MOVE ZEROS                    TO WS-DIGITO
               END-IF

               IF WS-DIGITO NOT EQUAL WS-DIG-CPF(10)
                   SET LK-VALIDO TO FALSE
               ELSE
                   COMPUTE WS-SOMA = (WS-DIG-CPF(01) * 11) +
                                     (WS-DIG-CPF(02) * 10) +
                                     (WS-DIG-CPF(03) *  9) +
                                     (WS-DIG-CPF(04) *  8) +
                                     (WS-DIG-CPF(05) *  7) +
                                     (WS-DIG-CPF(06) *  6) +
                                     (WS-DIG-CPF(07) *  5) +
                                     (WS-DIG-CPF(08) *  4) +
                                     (WS-DIG-CPF(09) *  3) +
                                     (WS-DIG-CPF(10) *  2)
                   DIVIDE WS-SOMA BY 11 GIVING    WS-QUOCIENTE
                                        REMAINDER WS-RESTO
                   IF WS-RESTO NOT LESS 02
                       SUBTRACT WS-RESTO FROM 11 GIVING WS-DIGITO
                   ELSE
                       MOVE ZEROS                TO WS-DIGITO
                   END-IF

                   IF WS-DIGITO NOT EQUAL WS-DIG-CPF(114)
                       SET LK-VALIDO TO FALSE
                   END-IF
               END-IF
           END-IF
           .
       0200-FIM.
           EXIT
           .
