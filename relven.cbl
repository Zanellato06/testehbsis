       IDENTIFICATION DIVISION.
       PROGRAM-ID.    RELVEN.
       SECURITY.
          *
          * Relatório de vendedores
          *
          **************************

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. PC.
       OBJECT-COMPUTER. PC.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "arqven.sel".
           SELECT ARQIMP ASSIGN TO PRINTER
                         FILE STATUS  IS WS-RESULTADO-ACESSO.

       DATA DIVISION.
       FILE SECTION.
           COPY "arqven.fd".
       FD  ARQIMP.
       01  REGTXT.
           05                      PIC X(132).

       WORKING-STORAGE SECTION.
           COPY "comum.wrk".
       01  WS-TECLA                PIC 9(002)             VALUE ZEROS.
           88  WS-ESC                                     VALUE 01.
                                                          FALSE 00.
       01  WS-OK                   PIC X(001)             VALUE SPACES.

       01  WS-ORDENACAO            PIC X(001)             VALUE SPACES.
       01  WS-CLASSIFICACAO        PIC X(001)             VALUE SPACES.
       01  WS-CODIGO-INI           PIC 9(007)             VALUE ZEROS.
       01  WS-CODIGO-FIM           PIC 9(007)             VALUE ZEROS.
       01  WS-NOME-INI             PIC X(040)             VALUE SPACES.
       01  WS-NOME-FIM             PIC X(040)             VALUE SPACES.

       01  WS-CODIGO-ED            PIC ZZ9                VALUE ZEROS.

       01  WS-LINHA                PIC 9(002)             VALUE ZEROS.
       01                          PIC 9(001)             VALUE ZEROS.
           88  WS-IMPRIMIR                                VALUE 01
                                                          FALSE 00.

       01  WS-CABECALHO01.
           05                      PIC X(106)             VALUE
               "Relatório de Vendedores".
           05                      PIC X(008)             VALUE
               "Página: ".
           05  CAB01-PAGINA        PIC 9(003)             VALUE ZEROS.
       01  WS-CABECALHO02.
           05                      PIC X(007)             VALUE
               "Código".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(040)             VALUE
               "Nome".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(014)             VALUE
               "CPF".
       01  WS-CABECALHO03.
           05                      PIC X(007)             VALUE ALL "-".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(040)             VALUE ALL "-".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(014)             VALUE ALL "-".
       01  WS-DETALHE.
           05                      PIC X(004)             VALUE SPACES.
           05  DET-CODIGO          PIC ZZ9                VALUE ZEROS.
           05                      PIC X(002)             VALUE SPACES.
           05  DET-NOME            PIC X(040)             VALUE SPACES.
           05                      PIC X(002)             VALUE SPACES.
           05  DET-CPF             PIC 999.999.999.99     VALUE ZEROS.

       SCREEN SECTION.
           COPY "relven.scr".

       PROCEDURE DIVISION.
       0000-PRINCIPAL SECTION.
       0000.
          PERFORM 0100-ABRIR-ARQUIVOS
          PERFORM 0200-TRATA-TELA
          PERFORM 0300-PROCESSA
          PERFORM 0400-FECHAR-ARQUIVOS
          .
       0000-FIM.
           EXIT PROGRAM
           STOP RUN
           .

       0100-ABRIR-ARQUIVOS SECTION.
       0100.
           OPEN I-O ARQVEN
           IF NOT WS-STATUS-OK
               EVALUATE TRUE
                   WHEN WS-FILE-NOT-FOUND
                       OPEN OUTPUT ARQVEN
                       CLOSE       ARQVEN
                       OPEN I-O    ARQVEN
                   WHEN WS-FILE-CONFLIT
                       DISPLAY ">> Existe um conflito nas especificaçõe"
                               "s do arquivo de vendedores. Verifique!"
                               AT 2310 WITH BEEP
                       GO 0000-FIM
                   WHEN OTHER
                       DISPLAY ">> Ocorreu um problema não previsto na "
                               "abertura do arquivo de vendedores com o"
                               " código '" WS-RESULTADO-ACESSO
                               "'. Verifique!" AT 2310 WITH BEEP
                       GO 0000-FIM
               END-EVALUATE
           END-IF

           OPEN OUTPUT ARQIMP
           IF NOT WS-STATUS-OK
               DISPLAY ">> Ocorreu um problema não previsto na "
                       "abertura do arquivo de impressão com o"
                       " código '" WS-RESULTADO-ACESSO
                       "'. Verifique!" AT 2310 WITH BEEP
               GO 0000-FIM
           END-IF
           .
       0100-FIM.
           EXIT
           .

       0200-TRATA-TELA SECTION.
       0200.
           DISPLAY SC-TELA-RELATORIO-VENDEDORES
           .
       0201-ORDENACAO.
           ACCEPT WS-ORDENACAO AT 0421 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0000-FIM
           END-IF

           IF WS-ORDENACAO NOT EQUAL "A" AND "a" AND "D" AND "d"
               DISPLAY ">>> Ordenação inválida. Verifique!" AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0201-ORDENACAO
           END-IF
           .
       0202-CLASSIFICACAO.
           ACCEPT WS-CLASSIFICACAO AT 0621 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0201-ORDENACAO
           END-IF

           IF WS-CLASSIFICACAO NOT EQUAL "C AND "c" AND "R" AND "r"
               DISPLAY ">>> Classificação inválida. Verifique!" AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0202-CLASSIFICACAO
           END-IF
           .
       0203-CODIGO-INI.
           MOVE WS-CODIGO-INI                   TO WS-CODIGO-ED
           ACCEPT WS-CODIGO-ED AT 0821 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0202-CLASSIFICACAO
           END-IF

           MOVE WS-CODIGO-ED                    TO WS-CODIGO-INI
           .
       0204-CODIGO-FIM.
           MOVE WS-CODIGO-FIM                   TO WS-CODIGO-ED
           ACCEPT WS-CODIGO-ED AT 0921 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0203-CODIGO-INI
           END-IF

           MOVE WS-CODIGO-ED                    TO WS-CODIGO-FIM

           IF WS-CODIGO-INI GREATER WS-CODIGO-FIM
               DISPLAY ">>> Faixa de códigos inválida. Verifique!"
                   AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0204-CODIGO-FIM
           END-IF
           .
       0205-NOME-INI.
           ACCEPT WS-NOME-INI AT 1121 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0204-CODIGO-FIM
           END-IF
           .
       0206-NOME-FIM.
           ACCEPT WS-NOME-FIM AT 1221 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0205-NOME-INI
           END-IF

           IF WS-NOME-INI GREATER WS-NOME-FIM
               DISPLAY ">>> Faixa de nomes inválida. Verifique!"
                   AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0206-NOME-FIM
           END-IF
           .
       0208-CONFIRMACAO.
           PERFORM WITH TEST AFTER
               UNTIL WS-OK EQUAL "S" OR "s" OR "N" OR "n"
                     WS-ESC
               ACCEPT WS-OK AT 0730 WITH AUTO
               ACCEPT WS-TECLA FROM ESCAPE KEY
           END-PERFORM

           IF WS-ESC OR WS-OK EQUAL "N" OR "n"
               GO 0206-NOME-FIM
           END-IF
       0200-FIM.
           EXIT
           .

       0300-PROCESSA SECTION.
       0300.
           MOVE ZEROS               TO CAB01-PAGINA
           MOVE 66                  TO WS-LINHA

           PERFORM 0500-POSICIONA

           IF WS-STATUS-OK
               PERFORM 0600-LER-ARQVEN
           END-IF

           PERFORM UNTIL WS-EOF
               SET WS-IMPRIMIR TO TRUE

               IF WS-CODIGO-INI NOT EQUAL ZEROS OR
                  WS-CODIGO-FIM NOT EQUAL ZEROS
                   IF VEN-CODIGO LESS    WS-CODIGO-INI OR
                                 GREATER WS-CODIGO-FIM
                       SET WS-IMPRIMIR TO FALSE
                   END-IF
               END-IF

               IF WS-NOME-INI NOT EQUAL SPACES OR
                  WS-NOME-FIM NOT EQUAL SPACES
                   IF VEN-NOME LESS    WS-NOME-INI OR
                               GREATER WS-NOME-FIM
                       SET WS-IMPRIMIR TO FALSE
                   END-IF
               END-IF

               IF WS-IMPRIMIR
                   MOVE VEN-CODIGO             TO DET-CODIGO
                   MOVE VEN-NOME               TO DET-NOME
                   MOVE VEN-CPF                TO DET-CPF

                   PERFORM 0700-IMPRIME-LINHA
               END-IF

               PERFORM 0600-LER-ARQVEN
           END-PERFORM
           .
       0300-FIM.
           EXIT
           .

       0400-FECHAR-ARQUIVOS SECTION.
       0400.
           CLOSE ARQVEN ARQIMP
           .
       0400-FIM.
           EXIT
           .

       0500-POSICIONA SECTION.
       0500.
           IF WS-ORDENACAO EQUAL "C" OR "c"
               IF WS-CLASSIFICACAO EQUAL "C" OR "c"
                   MOVE WS-CODIGO-INI    TO VEN-CODIGO
                   START ARQVEN KEY NOT LESS VEN-CODIGO
               ELSE
                   MOVE WS-NOME-INI      TO VEN-RAZAO
                   START ARQVEN KEY NOT LESS VEN-RAZAO
               END-IF
           ELSE
               IF WS-CLASSIFICACAO EQUAL "C" OR "c"
                   IF WS-CODIGO-FIM NOT EQUAL ZEROS
                       MOVE WS-CODIGO-FIM    TO VEN-CODIGO
                   ELSE
                       MOVE HIGH-VALUES      TO VEN-CODIGO
                   END-IF
                   START ARQVEN KEY NOT GREATER VEN-CODIGO
               ELSE
                   IF WS-RAZAO-FIM NOT EQUAL SPACES
                       MOVE WS-NOME-FIM      TO VEN-RAZAO
                   ELSE
                       MOVE HIGH-VALUES      TO VEN-RAZAO
                   END-IF
                   START ARQVEN KEY NOT GREATER VEN-RAZAO
               END-IF
           END-IF
           .
       0500-FIM.
           EXIT
           .

       0600-LER-ARQVEN SECTION.
       0600.
           READ ARQVEN NEXT RECORD WITH NO LOCK
           .
       0600-FIM.
           EXIT
           .

       0700-IMPRIME-LINHA SECTION.
       0700.
           ADD 01  TO  WS-LINHA
           IF WS-LINHA GREATER 66
               ADD 01  TO  CAB01-PAGINA

               WRITE REGIMP FROM WS-CABECALHO01 AFTER PAGE
               WRITE REGIMP FROM WS-CABECALHO02 AFTER 02
               WRITE REGIMP FROM WS-CAVECALHO03 AFTER 01

               MOVE 05                          TO WS-LINHA
           END-IF

           WRITE REGIMP FROM WS-DETALHE AFTER 01
           .
       0700-FIM.
           EXIT
           .
