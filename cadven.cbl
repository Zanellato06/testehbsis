       IDENTIFICATION DIVISION.
       PROGRAM-ID.    CADVEN.
       SECURITY.
          *
          * Cadastro de vendedores
          *
          *************************

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. PC.
       OBJECT-COMPUTER. PC.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "arqven.sel".
           SELECT ARQTXT ASSIGN TO DISK
                         ORGANIZATION IS LINE SEQUENTIAL
                         ACCESS MODE  IS SEQUENTIAL
                         FILE STATUS  IS WS-RESULTADO-ACESSO.
           SELECT ARQOUT ASSIGN TO DISK
                         ORGANIZATION IS LINE SEQUENTIAL
                         ACCESS MODE  IS SEQUENTIAL
                         FILE STATUS  IS WS-RESULTADO-ACESSO.

       DATA DIVISION.
       FILE SECTION.
           COPY "arqven.fd".
       FD  ARQTXT
           LABEL RECORD IS STANDARD
           VALUE OF FILE-ID WS-ARQTXT.
       01  REGTXT.
           05  TXT-CODIGO          PIC 9(003).
           05  TXT-CPF             PIC 9(011).
           05  TXT-NOME            PIC X(040).
           05  TXT-LATITUDE        PIC S9(03)V9(008).
           05  TXT-LONGITUDE       PIC S9(03)V9(008).

       FD  ARQOUT
           LABEL RECORD IS STANDARD
           VALUE OF FILE-ID WS-ARQOUT.
       01  REGOUT.
           05  OUT-LINHA           PIC X(132).

       WORKING-STORAGE SECTION.
           COPY "comum.wrk".
       01  WS-TECLA                PIC 9(002)             VALUE ZEROS.
           88  WS-ESC                                     VALUE 01.
           88  WS-F09                                     VALUE 10.
           88  WS-F10                                     VALUE 11.
       01  WS-ACAO                 PIC 9(001)             VALUE ZEROS.
           88  WS-INCLUINDO                               VALUE 01.
           88  WS-ALTERANDO                               VALUE 02.
           88  WS-EXCLUIDO                                VALUE 03.
           88  WS-IMPORTANDO                              VALUE 04.
       01  WS-TESTE-CPF            PIC 9(001)             VALUE ZEROS.
           88  WS-CPF-DUPLICADO                           VALUE 01
                                                          FALSE 00.
       01  WS-OK                   PIC X(001)             VALUE SPACES.
       01  WS-NOME-ARQUIVO         PIC X(040)             VALUE SPACES.
       01  WS-ARQTXT               PIC X(040)             VALUE SPACES.
       01  WS-ARQOUT               PIC X(040)             VALUE SPACES.

       01  WS-LINHA                PIC 9(004)             VALUE ZEROS.
       01  WS-IMPORTADOS           PIC 9(004)             VALUE ZEROS.
       01  WS-REJEITADOS           PIC 9(004)             VALUE ZEROS.
       01  WS-STATUS-LINHA         PIC 9(001)             VALUE ZEROS.
           88  WS-IMPORTAR                                VALUE 01
                                                          FALSE 00.
       01  WS-TEXTO-REJEICAO.
           05                      PIC X(007)             VALUE 
                                                          "Linha:".
           05  WS-TR-LINHA         PIC ZZZ9               VALUE ZEROS.
           05                      PIC X(011)             VALUE 
                                                          " - Código: ".
           05  WS-TR-CODIGO        PIC ZZ9                VALUE ZEROS.
           05                      PIC X(011)             VALUE 
                                                          " - Motivo: ".
           05  WS-TR-MOTIVO        PIC X(090)             VALUE SPACES.

       01  WS-AUXILIARES.
           05  WS-CODIGO           PIC 9(003)             VALUE ZEROS.
           05  WS-CPF              PIC 9(011)             VALUE ZEROS.
           05  WS-NOME             PIC X(040)             VALUE SPACES.
           05  WS-LATITUDE         PIC S9(03)V9(008)      VALUE ZEROS.
           05  WS-LONGITUDE        PIC S9(03)V9(008)      VALUE ZEROS.

       01  WS-EDITADAS.
           05  WS-ED-CODIGO        PIC ZZ9                VALUE ZEROS.
           05  WS-ED-CPF           PIC ZZZ.ZZZ.ZZZ.ZZ     VALUE ZEROS.
           05  WS-ED-LATITUDE      PIC ---9,99999999      VALUE ZEROS.
           05  WS-ED-LONGITUDE     PIC ---9,99999999      VALUE ZEROS.

       SCREEN SECTION.
           COPY "cadven.scr".

       PROCEDURE DIVISION.
       0000-PRINCIPAL SECTION.
       0000.
          PERFORM 0100-MONTA-TELA
          PERFORM 0200-ABRIR-ARQUIVOS
          PERFORM 0300-PROCESSA WITH TEST AFTER UNTIL WS-ESC
          PERFORM 0400-FECHAR-ARQUIVOS
          .
       0000-FIM.
           EXIT PROGRAM
           STOP RUN
           .

       0100-MONTA-TELA SECTION.
       0100.
           DISPLAY SC-TELA-CADASTRO-VENDEDORES
           .
       0100-FIM.
           EXIT
           .

       0200-ABRIR-ARQUIVOS SECTION.
       0200.
           OPEN I-O ARQVEN
           IF NOT WS-STATUS-OK
               EVALUATE TRUE
                   WHEN WS-FILE-NOT-FOUND
                       OPEN OUTPUT ARQVEN
                       CLOSE       ARQVEN
                       OPEN I-O    ARQVEN
                   WHEN WS-FILE-CONFLIT
                       DISPLAY ">> Existe um conflito nas especificaçõe"
                               "s do arquivo de vendedores. Verifique!"
                               AT 2310 WITH BEEP
                       GO 0000-FIM
                   WHEN OTHER
                       DISPLAY ">> Ocorreu um problema não previsto na "
                               "abertura do arquivo de vendedores com o"
                               " código '" WS-RESULTADO-ACESSO
                               "'. Verifique!" AT 2310 WITH BEEP
                       GO 0000-FIM
               END-EVALUATE
           END-IF
           .
       0200-FIM.
           EXIT
           .

       0300-PROCESSA SECTION.
       0300.
           PERFORM 0800-INICIALIZA
           PERFROM 10OO-LIMPAR-TELA

           DISPLAY "Pressione <F10> para importar um arquivo" 
               AT 0430 WITH HIGHLIGHT

           MOVE ZEROS                      TO WS-ED-CODIGO
           ACCEPT WS-ED-CODIGO AT 0421 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0300-FIM
           END-IF
           IF WS-F10
               PERFORM 1200-IMPORTAR
               GO 0300-FIM
           END-IF

           MOVE WS-ED-CODIGO               TO WS-CODIGO
           MOVE WS-CODIGO                  TO VEN-CODIGO
           PERFORM 0500-LER

           IF WS-INCLUINDO
               PERFROM 10OO-LIMPAR-TELA
               DISPLAY WS-ED-CODIGO AT 0421
           ELSE
               PERFORM 1100-MOSTRAR-DADOS
               DISPLAY "Pressione <F9> para excluir" AT 0641 
                   WITH HIGHLIGHT
           END-IF
           .
       0301-CPF.
           MOVE WS-CPF                     TO WS-ED-CNPJ
           ACCEPT WS-ED-CPF AT 0621 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0300
           END-IF
           IF WS-F9 AND WS-ALTERANDO
               PERFORM 0800-DELETAR
               IF WS-EXCLUIDO
                   GO 0300-FIM
               END-IF
           END-IF

           MOVE WS-ED-CPF                  TO WS-CPF

           SET LK-VALIDAR-CPF TO TRUE
           MOVE WS-CPF                     TO LK-VALOR
           SET LK-VALIDO TO FALSE
           CALL   "VALIDAR" USING LK-PARAMETROS
           CANCEL "VALIDAR"

           IF NOT LK-VALIDO
               DISPLAY ">>> CPF inválido. Verifique!" AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0301-CPF
           END-IF

           MOVE WS-CPF                      TO VEN-CPF
           PERFORM 0505-LER-CPF
           IF WS-CPF-DUPLICADO
               DISPLAY ">>> CPF já está associado a outro vendedor. "
                       "Verifique!" AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0301-CPF
           END-IF
           .
       0302-NOME.
           ACCEPT WS-NOME AT 0821 WITH AUTO-SKIP
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0301-CPF
           END-IF

           IF WS-NOME EQUAL SPACES
               DISPLAY ">>> O nome do vendedor deve ser informado. "
                       "Verifique!" AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0302-NOME
           END-IF
           .
       0303-LATITUDE.
           MOVE WS-LATITUDE              TO WS-ED-LATITUDE
           ACCEPT WS-ED-LATITUDE AT 0821 WITH AUTO-SKIP
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0302-NOME
           END-IF

           MOVE WS-ED-LATITUDE            TO WS-LATITUDE
           .
       0304-LONGITUDE.
           MOVE WS-LONGITUDE             TO WS-ED-LONGITUDE
           ACCEPT WS-ED-LONGITUDE AT 0821 WITH AUTO-SKIP
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0303-LATITUDE
           END-IF

           MOVE WS-ED-LONGITUDE           TO WS-LONGITUDE
           .
       0305-CONFIRMA.
           DISPLAY SC-LIMPA-MSG
           DISPALY ">>> Confirma os dados? [ ]" AT 2310

           PERFORM WITH TEST AFTER
               UNTIL WS-OK EQUAL "S" OR "s" OR 
                                 "N" OR "n"
               MOVE SPACES                TO WS-OK
               ACCEPT WS-OK AT 2334
           END-PERFORM

           IF WS-OK EQUAL "S" OR "s"
               IF WS-INCLUINDO
                   PERFORM 0600-GRAVAR
               ELSE
                   PREFORM 0700-REGRAVAR
               END-IF
           ELSE
               GO 0301-CPF
           END-IF
       0300-FIM.
           EXIT
           .

       0400-FECHAR-ARQUIVOS SECTION.
       0400.
           CLOSE ARQVEN
           .
       0400-FIM.
           EXIT
           .

       0500-LER SECTION.
       0500.
           READ ARQVEN WITH NO LOCK INVALID KEY
               SET WS-INCLUINDO TO TRUE
           NOT INVALID KEY
               SET WS-ALTERANDO TO TRUE
           END-READ
           .
       0500-FIM.
           EXIT
           .

       0505-LER-CPF SECTION.
       0505.
           SET WS-CPF-DUPLICADO TO FALSE

           READ ARQVEN WITH NO LOCK KEY VEN-CPF NOT INVALID KEY
               IF VEN-CODIGO NOT EQUAL WS-CODIGO OR WS-IMPORTANDO
                   SET WS-CPF-DUPLICADO TO TRUE
               END-IF
           END-READ
           .
       0505-FIM.
           EXIT
           .

       0600-GRAVAR SECTION.
       0600.
           MOVE WS-CODIGO                  TO VEN-CODIGO
           MOVE WS-CPF                     TO VEN-CPF
           MOVE WS-NOME                    TO VEN-NOME
           MOVE WS-LATITUDE                TO VEN-LATITUDE
           MOVE WS-LONGITUDE               TO VEN-LONGITUDE

           WRITE REGVEN INVALID KEY
               DISPLAY ">> Ocorreu um problema ao efetuar uma gravação "
                       "do arquivo de vendedores com o código '" 
                       WS-RESULTADO-ACESSO "'. Verifique!" 
                       AT 2310 WITH BEEP
               PERFORM 0400-FECHAR-ARQUIVOS
               GO 0000-FIM
           END-WRITE
           .
       0600-FIM.
           EXIT
           .

       0700-REGRAVAR SECTION.
       0700.
           MOVE WS-CODIGO                  TO VEN-CODIGO
           PERFORM 0500-LER
           IF WS-STATUS-OK
               MOVE WS-CPF                 TO VEN-CPF
               MOVE WS-NOME                TO VEN-NOME
               MOVE WS-LATITUDE            TO VEN-LATITUDE
               MOVE WS-LONGITUDE           TO VEN-LONGITUDE

               REWRITE REGVEN INVALID KEY
                   DISPLAY ">> Ocorreu um problema ao efetuar uma "
                           "atualização de dados no arquivo de "
                           "vendedores com o código '" 
                           WS-RESULTADO-ACESSO "'. Verifique!" 
                           AT 2310 WITH BEEP
                   PERFORM 0400-FECHAR-ARQUIVOS
                   GO 0000-FIM
               END-REWRITE
           END-IF
           .
       0700-FIM.
           EXIT
           .

       0800-DELETAR SECTION.
       0800.
           PERFORM WITH TEST AFTER 
               UNTIL WS-OK EQUAL "S" OR "s" OR 
                                 "N" OR "n"
               DISPLAY "Confirma a exclusão do vendedor? [ ]" AT 2310
               ACCEPT WS-OK AT 2343
           END-PERFORM

           IF WS-OK EQUAL "S" OR "s"
               DELETE ARQVEN INVALID KEY
                   DISPLAY ">> Ocorreu um problema ao efetuar uma "
                           "exclusão do arquivo de vendedores com "
                           "o código '" WS-RESULTADO-ACESSO "'. 
                           Verifique!" AT 2310 WITH BEEP
                   PERFORM 0400-FECHAR-ARQUIVOS
                   GO 0000-FIM
               NOT INVALID KEY
                   SET WS-EXCLUIDO TO TRUE
                   PERFORM 0900-INICIALIZA
                   PERFORM 1000-LIMPAR-TELA
               END-DELETE
           END-IF
           .
       0800-FIM.
           EXIT
           .

       0900-INICIALIZA SECTION.
       0900.
           MOVE ZEROS                      TO WS-CODIGO
                                              WS-CPF
                                              WS-LATITUDE
                                              WS-LONGITUDE
           MOVE SPACES                     TO WS-NOME
           .
       0900-FIM.
           EXIT
           .

       1000-LIMPAR-TELA SECTION.
       1000.
           DISPLAY SC-LIMPA-CAMPOS
           .
       1000-FIM.
           EXIT
           .

       1100-MOSTRAR-DADOS SECTION.
       1100.
           MOVE VEN-CODIGO                 TO WS-CODIGO
                                              WS-ED-CODIGO
           MOVE VEN-CNPJ                   TO WS-CPF
                                              WS-ED-CPF
           MOVE VEN-NOME                   TO WS-NOME
           MOVE VEN-LATITUDE               TO WS-LATITUDE
                                              WS-ED-LATITUDE
           MOVE VEN-LONGITUDE              TO WS-LONGITUDE
                                              WS-ED-LONGITUDE

           DISPLAY WS-ED-CODIGO    AT 0421
           DISPLAY WS-ED-CPF       AT 0621
           DISPLAY WS-NOME         AT 0821
           DISPLAY WS-ED-LATITUDE  AT 1021
           DISPLAY WS-ED-LONGITUDE AT 1221
           .
       1100-FIM.
           EXIT
           .

       1200-IMPORTAR SECTION.
       1200.
           DISPLAY SC-LIMPA-MSG
           DISPLAY ">> Informe o nome do arquivo: [" AT 2305
           DISPLAY "]" AT 2376

           PERFORM WITH TEST AFTER 
               UNTIL WS-NOME-ARQUIVO NOT EQUAL SPACES OR WS-ESC
               MOVE SPACES                 TO WS-NOME-ARQUIVO
               ACCEPT WS-NOME-ARQUIVO AT 2336 WITH AUTO
               ACCEPT WS-TECLA FROM ESCAPE KEY
           END-PERFORM

           IF WS-ESC
               GO 1200-FIM
           END-IF
           .
       1201-ABRE-ARQUIVO.
           MOVE WS-NOME-ARQUIVO            TO WS-ARQENTR
           OPEN INPUT ARQENTR
           IF NOT WS-STATUS-OK
               EVALUATE TRUE
                   WHEN WS-FILE-NOT-FOUND
                       DISPLAY ">> O arquivo informado não foi "
                               "localizado. Verifique!" AT 2310
                               WITH BEEP
                       ACCEPT WS-OK AT 2379 WITH NO-ECHO
                       GO 1200-FIM
                   WHEN OTHER
                       DISPLAY ">> Ocorreu um problema não previsto na "
                               "abertura do arquivo de importação com o"
                               " código '" WS-RESULTADO-ACESSO 
                               "'. Verifique!" AT 2310 WITH BEEP
                       ACCEPT WS-OK AT 2379 WITH NO-ECHO
                       GO 1200-FIM
               END-EVALUATE
           END-IF
           .
       1202-CONFIRMA.
           DISPLAY SC-LIMPA-MSG
           DISPALY ">>> Confirma a importação? [ ]" AT 2310

           PERFORM WITH TEST AFTER
               UNTIL WS-OK EQUAL "S" OR "s" OR
                                 "N" OR "n"
               MOVE SPACES                TO WS-OK
               ACCEPT WS-OK AT 2338
           END-PERFORM

           IF WS-OK EQUAL "N" OR "n"
               CLOSE ARQTXT
               GO 1200-FIM
           END-IF
           .
       1203-PROCESSAMENTO.
           SET WS-IMPORTANDO TO TRUE

           MOVE ZEROS                     TO WS-LINHA
                                             WS-IMPORTADOS
                                             WS-REJEITAD0S

           MOVE SPACES                    TO WS-ARQOUT
           STRING WS-NOME-ARQUIVO DELIMITED BY SPACES
                  ".out"                INTO WS-ARQOUT
           OPEN OUTPUT ARQOUT

           PERFORM WITH TEST AFTER UNTIL WS-EOF
               READ ARQTXT NEXT RECORD NOT AT END
                   ADD 01  TO  WS-LINHA
    
                   SET WS-IMPORTAR TO TRUE
    
                   MOVE TXT-CODIGO            TO VEN-CODIGO
                   PERFORM 0500-LER
                   IF WS-ALTERANDO
                       MOVE TXT-CODIGO        TO WS-TR-CODIGO
                       MOVE "Código já cadastrado."
                                              TO WS-TR-MOTIVO
                       PERFORM 1300-GRAVA-REJEICAO
                   END-IF
    
                   SET LK-VALIDAR-CPF TO TRUE
                   MOVE TXT-CPF               TO LK-VALOR
                   SET LK-VALIDO TO FALSE
                   CALL   "VALIDAR" USING LK-PARAMETROS
                   CANCEL "VALIDAR"
    
                   IF NOT LK-VALIDO
                       MOVE TXT-CODIGO        TO WS-TR-CODIGO
                       MOVE "CPF inválido."
                                              TO WS-TR-MOTIVO
                       PERFORM 1300-GRAVA-REJEICAO
                   END-IF
    
                   MOVE TXT-CPF               TO VEN-CPF
                   PERFORM 0505-LER-CPF
                   IF WS-CPF-DUPLICADO
                       MOVE TXT-CODIGO        TO WS-TR-CODIGO
                       MOVE "CPF já está associado a um vendedor."
                                              TO WS-TR-MOTIVO
                       PERFORM 1300-GRAVA-REJEICAO
                   END-IF

                   IF TXT-NONE EQUAL SPACES
                       MOVE TXT-CODIGO        TO WS-TR-CODIGO
                       MOVE "Nome não informado."
                                              TO WS-TR-MOTIVO
                       PERFORM 1300-GRAVA-REJEICAO
                   END-IF

                   IF WS-IMPORTAR
                       ADD 01  TO  WS-IMPORTADOS

                       MOVE TXT-CODIGO        TO WS-CODIGO
                       MOVE TXT-CPF           TO WS-CPF
                       MOVE TXT-NOME          TO WS-NOME
                       MOVE TXT-LATITUDE      TO WS-LATITUDE
                       MOVE TXT-LONGITUDE     TO WS-LONGITUDE

                       PERFORM 0600-GRAVAR
                   ELSE
                       ADD 01  TO  WS-REJEITADOS
                   END-IF
               END-READ
           END-PERFORM
           .
       1204-FINALIZA.
           CLOSE ARQTXT ARQOUT

           DISPLAY ">>> Importação concluída. Lidos: "
                   WS-LINHA " Efetivados: "
                   WS-IMPORTADOS " Rejeitados: "
                   WS-REJEITADOS AT 2310 WITH BEEP
           ACCEPT WS-OK AT 2379 WITH NO-ECHO
           .
       1200-FIM.
           EXIT
           .

       1300-GRAVA-REJEICAO SECTION.
       1300.
           MOVE WS-LINHA                  TO WS-TR-LINHA
           WRITE REGOUT FROM WS-TEXTO-REJEICAO AFTER 01

           SET WS-IMPORTAR TO FALSE
           .
       1300-FIM.
           EXIT
           .
