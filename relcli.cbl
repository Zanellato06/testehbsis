       IDENTIFICATION DIVISION.
       PROGRAM-ID.    RELCLI.
       SECURITY.
          *
          * Relatório de clientes
          *
          ************************

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. PC.
       OBJECT-COMPUTER. PC.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "arqcli.sel".
           COPY "arqven.sel".
           SELECT ARQIMP ASSIGN TO PRINTER
                         FILE STATUS  IS WS-RESULTADO-ACESSO.

       DATA DIVISION.
       FILE SECTION.
           COPY "arqcli.fd".
           COPY "arqven.fd".
       FD  ARQIMP.
       01  REGTXT.
           05                      PIC X(132).

       WORKING-STORAGE SECTION.
           COPY "comum.wrk".
       01  WS-TECLA                PIC 9(002)             VALUE ZEROS.
           88  WS-ESC                                     VALUE 01.
                                                          FALSE 00.
       01  WS-OK                   PIC X(001)             VALUE SPACES.

       01  WS-ORDENACAO            PIC X(001)             VALUE SPACES.
       01  WS-CLASSIFICACAO        PIC X(001)             VALUE SPACES.
       01  WS-CODIGO-INI           PIC 9(007)             VALUE ZEROS.
       01  WS-CODIGO-FIM           PIC 9(007)             VALUE ZEROS.
       01  WS-RAZAO-INI            PIC X(040)             VALUE SPACES.
       01  WS-RAZAO-FIM            PIC X(040)             VALUE SPACES.
       01  WS-VENDEDOR             PIC 9(003)             VALUE ZEROS.

       01  WS-VEND                 PIC 9(003)             VALUE ZEROS.
       01  WS-NOME-VEND            PIC X(040)             VALUE SPACES.
       01  WS-MENOR-DISTANCIA      PIC 9(005)V9(003)      VALUE ZEROS.
       01  WS-DISTANCIA            PIC 9(005)V9(003)      VALUE ZEROS.

       01  WS-CODIGO-ED            PIC ZZZZZZ9            VALUE ZEROS.
       01  WS-COD-VEND-ED          PIC ZZ9                VALUE ZEROS.

       01  WS-LINHA                PIC 9(002)             VALUE ZEROS.
       01                          PIC 9(001)             VALUE ZEROS.
           88  WS-IMPRIMIR                                VALUE 01
                                                          FALSE 00.

       01  WS-CABECALHO01.
           05                      PIC X(106)             VALUE
               "Relatório de Clientes".
           05                      PIC X(008)             VALUE
               "Página: ".
           05  CAB01-PAGINA        PIC 9(003)             VALUE ZEROS.
       01  WS-CABECALHO02.
           05                      PIC X(007)             VALUE
               "Código".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(040)             VALUE
               "Razão Social".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(018)             VALUE
               "CNPJ".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(046)             VALUE
               "Vendedor".
       01  WS-CABECALHO03.
           05                      PIC X(007)             VALUE ALL "-".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(040)             VALUE ALL "-".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(018)             VALUE ALL "-".
           05                      PIC X(002)             VALUE SPACES.
           05                      PIC X(046)             VALUE ALL "-".
       01  WS-DETALHE.
           05  DET-CODIGO          PIC ZZZZZZ9            VALUE ZEROS.
           05                      PIC X(002)             VALUE SPACES.
           05  DET-RAZAO           PIC X(040)             VALUE SPACES.
           05                      PIC X(002)             VALUE SPACES.
           05  DET-CNPJ            PIC 99.999.999/9999.99 VALUE ZEROS.
           05                      PIC X(002)             VALUE SPACES.
           05  DET-COD-VEND        PIC 9(003)             VALUE ZEROS.
           05                      PIC X(003)             VALUE " - ".
           05  DET-NOME-VEND       PIC X(040)             VALUE SPACES.

       SCREEN SECTION.
           COPY "relcli.scr".

       PROCEDURE DIVISION.
       0000-PRINCIPAL SECTION.
       0000.
          PERFORM 0100-ABRIR-ARQUIVOS
          PERFORM 0200-TRATA-TELA
          PERFORM 0300-PROCESSA
          PERFORM 0400-FECHAR-ARQUIVOS
          .
       0000-FIM.
           EXIT PROGRAM
           STOP RUN
           .

       0100-ABRIR-ARQUIVOS SECTION.
       0100.
           OPEN I-O ARQCLI
           IF NOT WS-STATUS-OK
               EVALUATE TRUE
                   WHEN WS-FILE-NOT-FOUND
                       OPEN OUTPUT ARQCLI
                       CLOSE       ARQCLI
                       OPEN I-O    ARQCLI
                   WHEN WS-FILE-CONFLIT
                       DISPLAY ">> Existe um conflito nas "
                               "especificações do arquivo de "
                               "clientes. Verifique!"
                               AT 2310 WITH BEEP
                       GO 0000-FIM
                   WHEN OTHER
                       DISPLAY ">> Ocorreu um problema não "
                               "previsto na abertura do arquivo de "
                               "clientes com o código '"
                               WS-RESULTADO-ACESSO "'. Verifique!"
                               AT 2310 WITH BEEP
                       GO 0000-FIM
               END-EVALUATE
           END-IF

           OPEN I-O ARQVEN
           IF NOT WS-STATUS-OK
               EVALUATE TRUE
                   WHEN WS-FILE-NOT-FOUND
                       OPEN OUTPUT ARQVEN
                       CLOSE       ARQVEN
                       OPEN I-O    ARQVEN
                   WHEN WS-FILE-CONFLIT
                       DISPLAY ">> Existe um conflito nas especificaçõe"
                               "s do arquivo de vendedores. Verifique!"
                               AT 2310 WITH BEEP
                       GO 0000-FIM
                   WHEN OTHER
                       DISPLAY ">> Ocorreu um problema não previsto na "
                               "abertura do arquivo de vendedores com o"
                               " código '" WS-RESULTADO-ACESSO
                               "'. Verifique!" AT 2310 WITH BEEP
                       GO 0000-FIM
               END-EVALUATE
           END-IF

           OPEN OUTPUT ARQIMP
           IF NOT WS-STATUS-OK
               DISPLAY ">> Ocorreu um problema não previsto na "
                       "abertura do arquivo de impressão com o"
                       " código '" WS-RESULTADO-ACESSO
                       "'. Verifique!" AT 2310 WITH BEEP
               GO 0000-FIM
           END-IF
           .
       0100-FIM.
           EXIT
           .

       0200-TRATA-TELA SECTION.
       0200.
           DISPLAY SC-TELA-RELATORIO-CLIENTES
           .
       0201-ORDENACAO.
           ACCEPT WS-ORDENACAO AT 0421 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0000-FIM
           END-IF

           IF WS-ORDENACAO NOT EQUAL "A" AND "a" AND "D" AND "d"
               DISPLAY ">>> Ordenação inválida. Verifique!" AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0201-ORDENACAO
           END-IF
           .
       0202-CLASSIFICACAO.
           ACCEPT WS-CLASSIFICACAO AT 0621 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0201-ORDENACAO
           END-IF

           IF WS-CLASSIFICACAO NOT EQUAL "C AND "c" AND "R" AND "r"
               DISPLAY ">>> Classificação inválida. Verifique!" AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0202-CLASSIFICACAO
           END-IF
           .
       0203-CODIGO-INI.
           MOVE WS-CODIGO-INI                   TO WS-CODIGO-ED
           ACCEPT WS-CODIGO-ED AT 0821 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0202-CLASSIFICACAO
           END-IF

           MOVE WS-CODIGO-ED                    TO WS-CODIGO-INI
           .
       0204-CODIGO-FIM.
           MOVE WS-CODIGO-FIM                   TO WS-CODIGO-ED
           ACCEPT WS-CODIGO-ED AT 0921 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0203-CODIGO-INI
           END-IF

           MOVE WS-CODIGO-ED                    TO WS-CODIGO-FIM

           IF WS-CODIGO-INI GREATER WS-CODIGO-FIM
               DISPLAY ">>> Faixa de códigos inválida. Verifique!"
                   AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0204-CODIGO-FIM
           END-IF
           .
       0205-RAZAO-INI.
           ACCEPT WS-RAZAO-INI AT 1121 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0204-CODIGO-FIM
           END-IF
           .
       0206-RAZAO-FIM.
           ACCEPT WS-RAZAO-FIM AT 1221 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0205-RAZAO-INI
           END-IF

           IF WS-RAZAO-INI GREATER WS-RAZAO-FIM
               DISPLAY ">>> Faixa de razão social inválida. Verifique!"
                   AT 2310
               ACCEPT WS-OK AT 2379 WITH NO-ECHO
               DISPLAY SC-LIMPA-MSG
               GO 0206-RAZAO-FIM
           END-IF
           .
       0207-VENDEDOR.
           MOVE WS-VENDEDOR                     TO WS-COD-VEND-ED
           ACCEPT WS-COD-VEND-ED AT 1421 WITH AUTO
           ACCEPT WS-TECLA FROM ESCAPE KEY

           IF WS-ESC
               GO 0206-RAZAO-FIM
           END-IF

           MOVE WS-COD-VEND-ED                  TO WS-VENDEDOR
                                                   VEN-CODIGO

           IF WS-VENDEDOR NOT EQUAL ZEROS
               READ ARQVEN WITH NO LOCK INVALID KEY
                   DISPLAY ">>> Vendedor não inválido Verifique!"
                       AT 2310
                   ACCEPT WS-OK AT 2379 WITH NO-ECHO
                   DISPLAY SC-LIMPA-MSG
                   GO 0207-VENDEDOR
               NOT INVALID KEY
                   DISPLAY VEN-NOME AT 1426
               END-READ
           ELSE
               MOVE SPACES             TO VEN-NOME
               DISPLAY VEN-NOME AT 1426
           END-IF
           .
       0208-CONFIRMACAO.
           PERFORM WITH TEST AFTER
               UNTIL WS-OK EQUAL "S" OR "s" OR "N" OR "n"
                     WS-ESC
               ACCEPT WS-OK AT 0730 WITH AUTO
               ACCEPT WS-TECLA FROM ESCAPE KEY
           END-PERFORM

           IF WS-ESC OR WS-OK EQUAL "N" OR "n"
               GO 0207-VENDEDOR
           END-IF
       0200-FIM.
           EXIT
           .

       0300-PROCESSA SECTION.
       0300.
           MOVE ZEROS               TO CAB01-PAGINA
           MOVE 66                  TO WS-LINHA

           PERFORM 0500-POSICIONA

           IF WS-STATUS-OK
               PERFORM 0600-LER-ARQCLI
           END-IF

           PERFORM UNTIL WS-EOF
               SET WS-IMPRIMIR TO TRUE

               PERFORM 0700-LOCALIZA-VENDEDOR

               IF WS-CODIGO-INI NOT EQUAL ZEROS OR
                  WS-CODIGO-FIM NOT EQUAL ZEROS
                   IF CLI-CODIGO LESS    WS-CODIGO-INI OR
                                 GREATER WS-CODIGO-FIM
                       SET WS-IMPRIMIR TO FALSE
                   END-IF
               END-IF

               IF WS-RAZAO-INI NOT EQUAL SPACES OR
                  WS-RAZAO-FIM NOT EQUAL SPACES
                   IF CLI-RAZAO LESS    WS-RAZAO-INI OR
                                GREATER WS-RAZAO-FIM
                       SET WS-IMPRIMIR TO FALSE
                   END-IF
               END-IF

               IF WS-VENDEDOR EQUAL ZEROS OR WS-VEND
                  SET WS-IMPRIMIR TO FALSE
               END-IF

               IF WS-IMPRIMIR
                   MOVE CLI-CODIGO             TO DET-CODIGO
                   MOVE CLI-RAZAO              TO DET-RAZAO
                   MOVE CLI-CNPJ               TO DET-CNPJ
                   MOVE WS-VENDEDOR            TO DET-COD-VEND
                   MOVE WS-NOME-VENDEDOR       TO DET-NOME-VEND

                   PERFORM 0900-IMPRIME-LINHA
               END-IF

               PERFORM 0600-LER-ARQCLI
           END-PERFORM
           .
       0300-FIM.
           EXIT
           .

       0400-FECHAR-ARQUIVOS SECTION.
       0400.
           CLOSE ARQCLI ARQVEN ARQIMP
           .
       0400-FIM.
           EXIT
           .

       0500-POSICIONA SECTION.
       0500.
           IF WS-ORDENACAO EQUAL "C" OR "c"
               IF WS-CLASSIFICACAO EQUAL "C" OR "c"
                   MOVE WS-CODIGO-INI    TO CLI-CODIGO
                   START ARQCLI KEY NOT LESS CLI-CODIGO
               ELSE
                   MOVE WS-RAZAO-INI     TO CLI-RAZAO
                   START ARQCLI KEY NOT LESS CLI-RAZAO
               END-IF
           ELSE
               IF WS-CLASSIFICACAO EQUAL "C" OR "c"
                   IF WS-CODIGO-FIM NOT EQUAL ZEROS
                       MOVE WS-CODIGO-FIM    TO CLI-CODIGO
                   ELSE
                       MOVE HIGH-VALUES      TO CLI-CODIGO
                   END-IF
                   START ARQCLI KEY NOT GREATER CLI-CODIGO
               ELSE
                   IF WS-RAZAO-FIM NOT EQUAL SPACES
                       MOVE WS-RAZAO-FIM     TO CLI-RAZAO
                   ELSE
                       MOVE HIGH-VALUES      TO CLI-RAZAO
                   END-IF
                   START ARQCLI KEY NOT GREATER CLI-RAZAO
               END-IF
           END-IF
           .
       0500-FIM.
           EXIT
           .

       0600-LER-ARQCLI SECTION.
       0600.
           READ ARQCLI NEXT RECORD WITH NO LOCK
           .
       0600-FIM.
           EXIT
           .


       0700-LOCALIZA-VENDEDOR SECTION.
       0700.
           MOVE ZEROS                          TO WS-VENDEDOR
           MOVE SPACES                         TO WS-NOME-VENDEDOR
           MOVE HIGH-VALUES                    TO WS-MENOR-DISTANCIA

           MOVE ZEROS                          TO VEN-CODIGO
           START ARQVEN KEY NOT LESS VEN-CODIGO NOT INVALID KEY
               PERFORM WITH TEST AFTER UNTIL WS-EOF
                   READ ARQVEN NEXT RECORD WITH NO LOCK NOT AT END
                       PERFORM 0800-CALCULA-DISTANCIA

                       IF WS-DISTANCIA LESS WS-MENOR-DISTANCIA
                           MOVE VEN-CODIGO         TO WS-VENDEDOR
                           MOVE VEN-NOME           TO WS-NOME-VENDEDOR
                           MOVE WS-DISTANCIA       TO WS-MENOR-DISTANCIA
                       END-IF
                   END-READ
               END-PERFORM
           END-START
           .
       0700-FIM.
           EXIT
           .

       0800-CALCULA-DISTANCIA SECTION.
       0800.
           COMPUTE WS-DLA ROUNDED =
               (CLI-LATITUDE - VEN-LATITUDE) / 60 * 1812
           COMPUTE WS-DLO ROUNDED =
               (CLI-LONGITUDE - VEN-LONGITUDE) / 60 * 1812

           MOVE FUNCTION SQRT((WS-DLA * WS-DLA) + (WS-DLO * WS-DLO))
                         TO WS-DISTANCIA
           .
       0800-FIM.
           EXIT
           .

       0900-IMPRIME-LINHA SECTION.
       0900.
           ADD 01  TO  WS-LINHA
           IF WS-LINHA GREATER 66
               ADD 01  TO  CAB01-PAGINA

               WRITE REGIMP FROM WS-CABECALHO01 AFTER PAGE
               WRITE REGIMP FROM WS-CABECALHO02 AFTER 02
               WRITE REGIMP FROM WS-CAVECALHO03 AFTER 01

               MOVE 05                          TO WS-LINHA
           END-IF

           WRITE REGIMP FROM WS-DETALHE AFTER 01
           .
       0900-FIM.
           EXIT
           .
