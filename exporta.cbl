       IDENTIFICATION DIVISION.
       PROGRAM-ID.    EXPORTAR.
       SECURITY.
          *
          * Gerar arquivo CSV de clientes
          *
          ********************************

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. PC.
       OBJECT-COMPUTER. PC.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "arqcli.sel".
           COPY "arqven.sel".
           SELECT ARQTXT ASSIGN TO DISK
                         ORGANIZATION IS LINE SEQUENTIAL
                         ACCESS MODE  IS SEQUENTIAL
                         FILE STATUS  IS WS-RESULTADO-ACESSO.

       DATA DIVISION.
       FILE SECTION.
           COPY "arqcli.fd".
           COPY "arqven.fd".
       FD  ARQTXT
           LABEL RECORD IS STANDARD
           VALUE OF FILE-ID WS-ARQTXT.
       01  REGTXT.
           05                      PIC X(132).

       WORKING-STORAGE SECTION.
           COPY "comum.wrk".
       01  WS-TECLA                PIC 9(002)             VALUE ZEROS.
           88  WS-ESC                                     VALUE 01.
                                                          FALSE 00.
       01  WS-OK                   PIC X(001)             VALUE SPACES.
       01  WS-ARQTXT               PIC X(040)             VALUE SPACES.

       01  WS-VENDEDOR             PIC 9(003)             VALUE ZEROS.
       01  WS-NOME-VENDEDOR        PIC X(040)             VALUE SPACES.
       01  WS-MENOR-DISTANCIA      PIC 9(005)V9(003)      VALUE ZEROS.
       01  WS-DISTANCIA            PIC 9(005)V9(003)      VALUE ZEROS.

       01  WS-TEXTO-CSV.
           05  WS-TC-CODIGO        PIC 9(007)             VALUE ZEROS.
           05                      PIC X(001)             VALUE ";".
           05  WS-TC-RAZAO         PIC X(040)             VALUE SPACES.
           05                      PIC X(001)             VALUE ";".
           05  WS-TC-VENDEDOR      PIC 9(003)             VALUE ZEROS.
           05                      PIC X(001)             VALUE ";".
           05  WS-TC-NOME-VEND     PIC X(040)             VALUE SPACES.
           05                      PIC X(001)             VALUE ";".
           05  WS-TC-DISTANCIA     PIC ZZ.ZZ9,999         VALUE ZEROS.

       SCREEN SECTION.
           COPY "exportar.scr".

       PROCEDURE DIVISION.
       0000-PRINCIPAL SECTION.
       0000.
          PERFORM 0100-TRATA-TELA
          PERFORM 0200-ABRIR-ARQUIVOS
          PERFORM 0300-PROCESSA
          PERFORM 0400-FECHAR-ARQUIVOS
          .
       0000-FIM.
           EXIT PROGRAM
           STOP RUN
           .

       0100-TRATA-TELA SECTION.
       0100.
           DISPLAY SC-TELA-EXPORTACAO
           .
       0101-NOME-ARQUIVO.
           PERFORM WITH TEST AFTER
               UNTIL WS-ARQTXT NOT EQUAL SPACES OR
                     WS-ESC
               ACCEPT WS-ARQTXT AT 0530 WITH AUTO
               ACCEPT WS-TECLA FROM ESCAPE KEY
           END-PERFORM

           IF WS-ESC
               GO 0000-FIM
           END-IF
           .
       0102.
           PERFORM WITH TEST AFTER
               UNTIL WS-OK EQUAL "S" OR "s" OR "N" OR "n"
                     WS-ESC
               ACCEPT WS-OK AT 0730 WITH AUTO
               ACCEPT WS-TECLA FROM ESCAPE KEY
           END-PERFORM

           IF WS-ESC OR WS-OK EQUAL "N" OR "n"
               GO 0101-NOME-ARQUIVO
           END-IF
       0100-FIM.
           EXIT
           .

       0200-ABRIR-ARQUIVOS SECTION.
       0200.
           OPEN I-O ARQCLI
           IF NOT WS-STATUS-OK
               EVALUATE TRUE
                   WHEN WS-FILE-NOT-FOUND
                       OPEN OUTPUT ARQCLI
                       CLOSE       ARQCLI
                       OPEN I-O    ARQCLI
                   WHEN WS-FILE-CONFLIT
                       DISPLAY ">> Existe um conflito nas "
                               "especificações do arquivo de "
                               "clientes. Verifique!"
                               AT 2310 WITH BEEP
                       GO 0000-FIM
                   WHEN OTHER
                       DISPLAY ">> Ocorreu um problema não "
                               "previsto na abertura do arquivo de "
                               "clientes com o código '"
                               WS-RESULTADO-ACESSO "'. Verifique!"
                               AT 2310 WITH BEEP
                       GO 0000-FIM
               END-EVALUATE
           END-IF

           OPEN I-O ARQVEN
           IF NOT WS-STATUS-OK
               EVALUATE TRUE
                   WHEN WS-FILE-NOT-FOUND
                       OPEN OUTPUT ARQVEN
                       CLOSE       ARQVEN
                       OPEN I-O    ARQVEN
                   WHEN WS-FILE-CONFLIT
                       DISPLAY ">> Existe um conflito nas especificaçõe"
                               "s do arquivo de vendedores. Verifique!"
                               AT 2310 WITH BEEP
                       GO 0000-FIM
                   WHEN OTHER
                       DISPLAY ">> Ocorreu um problema não previsto na "
                               "abertura do arquivo de vendedores com o"
                               " código '" WS-RESULTADO-ACESSO
                               "'. Verifique!" AT 2310 WITH BEEP
                       GO 0000-FIM
               END-EVALUATE
           END-IF

           OPEN OUTPUT ARQTXT
           IF NOT WS-STATUS-OK
               DISPLAY ">> Ocorreu um problema não previsto na "
                       "abertura do arquivo a ser gerado com o"
                       " código '" WS-RESULTADO-ACESSO
                       "'. Verifique!" AT 2310 WITH BEEP
               GO 0000-FIM
           END-IF
           .
       0200-FIM.
           EXIT
           .

       0300-PROCESSA SECTION.
       0300.
           MOVE ZEROS                          TO CLI-CODIGO
           STRAT ARQCLI KEY NOT LESS CLI-CODIGO NOT INVALID KEY
               READ ARQCLI NEXT RECORD WITH NO LOCK NOT AT END
                   PERFORM 0500-LOCALIZA-VENDEDOR

                   MOVE CLI-CODIGO             TO WS-TC-CODIGO
                   MOVE CLI-RAZAO              TO WS-TC-RAZAO
                   MOVE WS-VENDEDOR            TO WS-TC-VENDEDOR
                   MOVE WS-NOME-VENDEDOR       TO WS-TC-NOME-VEND
                   MOVE WS-MENOR-DISTANCIA     TO WS-TC-DISTANCIA

                   PERFORM 0700-GRAVAR-CSV
               END-READ
           END-START
           .
       0300-FIM.
           EXIT
           .

       0400-FECHAR-ARQUIVOS SECTION.
       0400.
           CLOSE ARQCLI ARQVEN ARQTXT
           .
       0400-FIM.
           EXIT
           .

       0500-LOCALIZA-VENDEDOR SECTION.
       0500.
           MOVE ZEROS                          TO WS-VENDEDOR
           MOVE SPACES                         TO WS-NOME-VENDEDOR
           MOVE HIGH-VALUES                    TO WS-MENOR-DISTANCIA

           MOVE ZEROS                          TO VEN-CODIGO
           START ARQVEN KEY NOT LESS VEN-CODIGO NOT INVALID KEY
               PERFORM WITH TEST AFTER UNTIL WS-EOF
                   READ ARQVEN NEXT RECORD WITH NO LOCK NOT AT END
                       PERFORM 0600-CALCULA-DISTANCIA

                       IF WS-DISTANCIA LESS WS-MENOR-DISTANCIA
                           MOVE VEN-CODIGO         TO WS-VENDEDOR
                           MOVE VEN-NOME           TO WS-NOME-VENDEDOR
                           MOVE WS-DISTANCIA       TO WS-MENOR-DISTANCIA
                       END-IF
                   END-READ
               END-PERFORM
           END-START
           .
       0500-FIM.
           EXIT
           .

       0600-CALCULA-DISTANCIA SECTION.
       0600.
           COMPUTE WS-DLA ROUNDED =
               (CLI-LATITUDE - VEN-LATITUDE) / 60 * 1812
           COMPUTE WS-DLO ROUNDED =
               (CLI-LONGITUDE - VEN-LONGITUDE) / 60 * 1812

           MOVE FUNCTION SQRT((WS-DLA * WS-DLA) + (WS-DLO * WS-DLO))
                         TO WS-DISTANCIA
           .
       0600-FIM.
           EXIT
           .

       0700-GRAVAR-CSV SECTION.
       0700.
           WRITE REGTXT FROM WS-TEXTO-CSV AFTER 01
           .
       0700-FIM.
           EXIT
           .
           